﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SatImgLoader
{
   static class CRequestParams
  {
    private static DateTime beginDate;
    private static DateTime endDate;
    private static int zoomLevel;
    private static int minCloudLevel;
    private static int maxCludLevel;
    private static int minResolution;
    private static int maxResolution;
    private static int minAngle;
    private static int maxAngle;
    public static double[] Coords = new double[10];

    public static DateTime BeginDate
    {
      get
      {
        return beginDate;
      }

      set
      {
        beginDate = value;
      }
    }

    public static DateTime EndDate
    {
      get
      {
        return endDate;
      }

      set
      {
        endDate = value;
      }
    }

    public static int ZoomLevel
    {
      get
      {
        return zoomLevel;
      }

      set
      {
        zoomLevel = value;
      }
    }

    public static int MinCloudLevel
    {
      get
      {
        return minCloudLevel;
      }

      set
      {
        minCloudLevel = value;
      }
    }

    public static int MaxCludLevel
    {
      get
      {
        return maxCludLevel;
      }

      set
      {
        maxCludLevel = value;
      }
    }

    public static int MinResolution
    {
      get
      {
        return minResolution;
      }

      set
      {
        minResolution = value;
      }
    }

    public static int MaxResolution
    {
      get
      {
        return maxResolution;
      }

      set
      {
        maxResolution = value;
      }
    }

    public static int MinAngle
    {
      get
      {
        return minAngle;
      }

      set
      {
        minAngle = value;
      }
    }

    public static int MaxAngle
    {
      get
      {
        return maxAngle;
      }

      set
      {
        maxAngle = value;
      }
    }
  }
}
