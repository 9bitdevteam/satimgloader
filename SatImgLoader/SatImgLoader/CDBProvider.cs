﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Windows;

namespace SatImgLoader
{
  public delegate void OnNewDbRecordAddedDelegate(Object info);
  public delegate void OnRecordRemovedDelegate(string id);
  static class CDBProvider
  {
    public static readonly string DataFolder = @"Data\";
    public static List<string> PicDataNames = new List<string>();
    public static event OnNewDbRecordAddedDelegate OnNewDbRecordAdded;
    public static event OnRecordRemovedDelegate OnRecordRemoved;
    public static void init()
    {
      if (!Directory.Exists(DataFolder)) Directory.CreateDirectory(DataFolder);
      string[] files = Directory.GetFiles(DataFolder, "*.data");
      PicDataNames.Clear();
      foreach (string dataFilename in files)
      {
        string dataFilenamel = dataFilename;
        string json = File.ReadAllText(dataFilenamel);
        Object root = JsonConvert.DeserializeObject<Object>(json);
        if (root == null) continue;
        string picfileName =  dataFilenamel.Replace(".data", "");
        if (!File.Exists(picfileName)) continue;
        PicDataNames.Add(dataFilenamel);

        OnNewDbRecordAdded?.Invoke(root);

      }
    }
    public static bool Add(Object picObj)
    {
      
      string picFileName = DataFolder + picObj.catalog_id + ".png";
      string DataFileName = picFileName + ".data";
      if(File.Exists(picFileName) && File.Exists(DataFileName))
      {
        MessageBox.Show("Такой снимок уже есть в базе");
        return false;
      }
      string json = JsonConvert.SerializeObject(picObj);
      if (File.Exists(picFileName))
      {

        File.WriteAllText(DataFileName, json);
        PicDataNames.Add(DataFileName);
        OnNewDbRecordAdded?.Invoke(picObj);
        return true;
      }
      else
        return false;
    }
    public static Object Get(string filePath)
    {
      Object info = null;
      string json = File.ReadAllText(filePath);
      info = JsonConvert.DeserializeObject<Object>(json);
      return info;
    }
    public static bool Remove(string fileName)
    {
      try
      {
        File.Delete(fileName);
        PicDataNames.RemoveAll(pdn =>fileName.Contains(pdn));
        fileName = fileName.Replace(".data", "");
        //File.Delete(fileName);
        return true;
      }
      catch(IOException e)
      {
        return false;
      }
    }
  }
}
