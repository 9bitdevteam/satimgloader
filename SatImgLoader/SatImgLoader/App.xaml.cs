﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace SatImgLoader
{
  /// <summary>
  /// Логика взаимодействия для App.xaml
  /// </summary>
  public partial class App : Application
  {
    private void Application_Startup(object sender, StartupEventArgs e)
    {
      if(DateTime.Now >= new DateTime(2017, 11, 30))
      {
        MessageBox.Show("Триальная версия приложения");
        Thread.Sleep(0);
        App.Current.Shutdown(0);
      } 
    }
  }
}
