﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SatImgLoader
{
  /// <summary>
  /// Логика взаимодействия для PictureReview.xaml
  /// </summary>
  public partial class PictureReview : Window
  {
    private Object info;
    string savePath;
    public PictureReview()
    {
      InitializeComponent();
    }
    public PictureReview(Object pictureInfo) : this()
    {
      info = pictureInfo;
    }
    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      int i = -1;
      Label[] lbls = new Label[13];
      Label l;
      l = new Label(); l.Content = "Данные о снимаке: ";
      l.FontWeight = FontWeights.Bold;
      lbls[++i] = l;

      l = new Label(); l.Content = "Каталожный номер ";
      lbls[++i] = l;

      l = new Label(); l.Content =  info.catalog_id;
      lbls[++i] = l;

      l = new Label(); l.Content = "Дата съемки " + info.date.ToShortDateString();
      lbls[++i] = l;

      l = new Label(); l.Content = "угол " + info.angle;
      lbls[++i] = l;

      l = new Label(); l.Content = "облачность " + info.cloudiness;
      lbls[++i] = l;

      l = new Label(); l.Content = "Данные о сенсоре:";
      l.FontWeight = FontWeights.Bold;
      lbls[++i] = l;

      l = new Label(); l.Content = "Имя спутника "  + info.sensor.satellite.display_name;
      lbls[++i] = l;

      l = new Label(); l.Content = "разрешение " + info.sensor.resolution;
      lbls[++i] = l;

      l = new Label(); l.Content = "тип снимка " + info.sensor.type.name;
      lbls[++i] = l;

      infoContainer.Children.Clear();
      foreach (Label lbl in lbls)
        if (lbl != null)
        infoContainer.Children.Add(lbl);
      
      
      
      //      infoContainer.Children.()




      string s = Directory.GetCurrentDirectory() + @"\" + info.id + ".png"; ;
      try
      {
        SatPicture.Source = new BitmapImage(new Uri(s, UriKind.RelativeOrAbsolute));
      }
      catch { }

    }
    private void LoadPicture()
    {
      string url = "";

      //default
      url = info.quicklook_url;

      if (info.quicklook_url.Contains("browse.digitalglobe.com"))
      {
        url = "https://browse.digitalglobe.com/imagefinder/showBrowseImage?";
        url += "catalogId=" + info.catalog_id + "&imageHeight=natres&imageWidth=natres";
        goto loadchek;
      }
      if (info.quicklook_url.EndsWith("png"))
      {
        url = info.quicklook_url;
        goto loadchek;
      }
      if (info.quicklook_url.Contains("catalog.sovzond.ru"))
      {
        url = info.quicklook_url;
        goto loadchek;
      }

      loadchek:
      if (url.Length < 10)
      {
        MessageBox.Show("Файл не найден на сервере");
        return;
      }
      EnableBar();
      WebClient Client = new WebClient();
      //Client.
      Client.DownloadProgressChanged += Client_DownloadProgressChanged;
      Client.DownloadFileCompleted += Client_DownloadFileCompleted;
      Client.DownloadFileAsync(new Uri(url), savePath);
    }

    private void Client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
   

      string filePath = savePath;
      if (!File.Exists(filePath))
      {
        return;
      }
      string argument = "/select, \"" + filePath + "\"";
      System.Diagnostics.Process.Start("explorer.exe", argument);

      MessageBox.Show("загрузка успешно завершена");
      if (CDBProvider.Add(info))
        MessageBox.Show("снимок успешно добавлен в базу данных");
      else
        MessageBox.Show("при добавлении в базу данных произошла ошибка");
    }

    private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
    {
      DownloadBar.Dispatcher.BeginInvoke(new Action(() => DownloadBar.Value = e.ProgressPercentage));
      Downloadstatus.Dispatcher.BeginInvoke(new Action(() =>
      {
        if (e.BytesReceived < e.TotalBytesToReceive)
          Downloadstatus.Content = "загружено " + (e.BytesReceived / 1024).ToString() + " / " + (e.TotalBytesToReceive / 1024).ToString() + " КB";
        else
          Downloadstatus.Content = "Снимок загружен полностью";
      }));
    }

    private void EnableBar()
    {
      DownloadBar.Dispatcher.BeginInvoke(new Action(() => DownloadBar.Visibility = Visibility.Visible));
    }

    private void DownloadPicBtn_Click(object sender, RoutedEventArgs e)
    {
      SaveFileDialog dialog = new SaveFileDialog();
      dialog.InitialDirectory = Directory.GetCurrentDirectory() + @"\" + CDBProvider.DataFolder;
      dialog.DefaultExt = "png|png";
      dialog.FileName = info.catalog_id + ".png";
      //dialog.ShowDialog();
      savePath = dialog.InitialDirectory + dialog.FileName;
      Task Loader = new Task(LoadPicture);
      Loader.Start();
    }
  }
}
