﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SatImgLoader
{
  /// <summary>
  /// Логика взаимодействия для DBExplorer.xaml
  /// </summary>
  public partial class DBExplorer : Window
  {
    public DBExplorer()
    {
      InitializeComponent();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      
      Task.Run(() => 
      {
        List<string> files = new List<string>();
        files.AddRange(CDBProvider.PicDataNames);
        List<Object> objlist = new List<Object>();
        for(int i = 0; i < files.Count(); i++)
        {
          Object info = CDBProvider.Get(files[i]);
          if (info == null) continue;
          CDBProvider_OnNewDbRecordAdded(info);
          objlist.Add(info);
        }
        DateTime MinDate = objlist.Select(x => x.date).Min();
        DateTime MaxDate = objlist.Select(x => x.date).Max();
        this.Dispatcher.BeginInvoke(new Action(() =>
        {
          BeginDate.SelectedDate = MinDate;
          BeginDate.DisplayDate = MinDate;

          EndDate.SelectedDate = MaxDate;
          EndDate.DisplayDate = MaxDate;
        }));
       

        CDBProvider.OnNewDbRecordAdded += CDBProvider_OnNewDbRecordAdded;

      });
      
    }

    private void CDBProvider_OnNewDbRecordAdded(Object info)
    {
      this.Dispatcher.BeginInvoke(new Action(() => 
      {
        DBRecordView view = new DBRecordView(info);
        view.OnRecordViewRemoved += View_OnRecordViewRemoved;
        DBRecordsContainer.Children.Add(view);
      }));
    
    }

    private void View_OnRecordViewRemoved(DBRecordView id)
    {
      DBRecordsContainer.Children.Remove(id);
    }

    private void MaxCloudScroll_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
      MaxCloudTB.Content = ((int)MaxCloudScroll.Value).ToString();
    }

    private void MaxAngleScroll_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
      MaxAngleTB.Content = ((int)MaxAngleScroll.Value).ToString();
    }

    private void SearchBtn_Click(object sender, RoutedEventArgs e)
    {
      int resultCount = 0;
      CDBProvider.OnNewDbRecordAdded -= CDBProvider_OnNewDbRecordAdded;
      foreach (var View in DBRecordsContainer.Children.OfType<DBRecordView>())
      {
        Object info = View.Info;
        if (info.date >= BeginDate.SelectedDate &&
           info.date <= EndDate.SelectedDate &&
           info.cloudiness <= this.MaxCloudScroll.Value &&
           double.Parse (info.angle.Replace(".",",")) <= this.MaxAngleScroll.Value
          )
        {
          View.Visibility = Visibility.Visible;
          resultCount++;
        }
        else
        {
          View.Visibility = Visibility.Collapsed;
        }
        if (resultCount == 0) MessageBox.Show("в базе нет снимков удовлетворяющих вабранным критериям");
      }
    }
  }
}
