﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SatImgLoader
{
  public static class CSatManager
  {
    private static List<CSatelite> _Satelites = new List<CSatelite>();
    private static bool isInited = false;
    static public List<CSatelite> Satelites
    {
      get
      {
        if (!isInited)
          Init();
        return _Satelites;
      }
      set { _Satelites = value; }
    }
    public static void Init()
    {
      isInited = true;
      Satelites.Clear();
      Satelites.Add(new CSatelite(0, false, "Alos Prism", "1"));
      Satelites.Add(new CSatelite(1, false, "Alos Avnir", "2"));
      Satelites.Add(new CSatelite(4, false, "БКА", "52"));
      Satelites.Add(new CSatelite(5, false, "Deimos-1", "41"));
      Satelites.Add(new CSatelite(6, false, "GeoEye", "6"));
      Satelites.Add(new CSatelite(7, false, "Ikonos", "9"));
      Satelites.Add(new CSatelite(8, false, "Канопус-В ПСС", "61"));
      Satelites.Add(new CSatelite(9, false, "Канопус-В МСС", "62"));
      Satelites.Add(new CSatelite(10, false, "KOMPSAT-2 ", "45"));
      Satelites.Add(new CSatelite(11, false, "KOMPSAT-3", "46"));
      Satelites.Add(new CSatelite(12, false, "KOMPSAT-3A ", "56"));
      Satelites.Add(new CSatelite(13, false, "Planet ", "57"));
      Satelites.Add(new CSatelite(14, false, "Pleiades", "42"));
      Satelites.Add(new CSatelite(15, false, "QuickBird ", "11"));
      Satelites.Add(new CSatelite(16, false, "RapidEye", "14"));
      Satelites.Add(new CSatelite(17, false, "Ресурс-П", "60"));
      Satelites.Add(new CSatelite(18, false, "Spot 6 ", "47"));
      Satelites.Add(new CSatelite(19, false, "WorldView-1 ", "32"));
      Satelites.Add(new CSatelite(20, false, "WorldView-2", "34"));
      Satelites.Add(new CSatelite(21, false, "WorldView-3 ", "44"));
    
    }
  }
  public class CSatelite
  {
    public int id;
    public bool IsSelected;
    public string Name;
    public string RequestName;
    public CSatelite()
    {

    }
    public CSatelite(int id, bool Selected, string Name, string RequestName) : this()
    {
      this.id = id;
      this.Name = Name;
      this.RequestName = RequestName;
    }
    public void Cb_Unchecked(object sender, RoutedEventArgs e)
    {
      IsSelected = false;
    }

    public void Cb_Checked(object sender, RoutedEventArgs e)
    {
      IsSelected = true;
    }

  }
}
