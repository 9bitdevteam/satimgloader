﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Windows;

namespace SatImgLoader
{
  public delegate void OnNewImageSetDelegate(int Count);
  public delegate void OnNewImageLoadedDelegate(string imagepath, Object sat);
  public delegate void OnLoadingFailedDelegate(string cause = "");

  class CImgLoader
  {
    private static string baseUrl = "http://catalog.sovzond.ru/";

    public static event OnNewImageSetDelegate OnNewImageSet;
    public static event OnNewImageLoadedDelegate OnNewImageLoaded;
    public static event OnLoadingFailedDelegate OnLoadingFailed;
    public static string BuildRequest()
    {
      string request = baseUrl;
      request += "api/v1/coverage/?";
      request += AddAngles();
      request += AddCloudness();
      request += AddDates();
      request += AddSattelites();
      request += AddGeometry();
      request += AddSeasons();
      request += AddResponseParams();
      return request;

    }

    public static void LoadData(string requeststring)
    {
     
      Rootobject root = null;
      try
      {
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requeststring);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader stream = new StreamReader(response.GetResponseStream());
        string json = stream.ReadToEnd();
        root = JsonConvert.DeserializeObject<Rootobject>(json);

      }
      catch (Exception e)
      {
        OnLoadingFailed?.Invoke();
        MessageBox.Show("Ошибка соединения с сервером\n" + e.Message);
        return;
      }
      if (root.meta.total_count == 0)
      {
        OnNewImageSet?.Invoke(0);
        MessageBox.Show("Снимков не найдено. Попробуйте другие критерии поиска");
        return;
      }
      if (MessageBox.Show("обнаружено " + root.meta.total_count + " снимков. Загрузить их?", "Внимание!", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
      {

        return;
      }

      CLocalimgDB.SatList.Clear();
      CLocalimgDB.SatList.AddRange(root.objects);
      OnNewImageSet?.Invoke(root.objects.Length);

      root.objects.ToList().OrderBy((x) =>x.date).AsParallel().ForAll((obj) =>
      {
        string baseurlload = "http://catalog.sovzond.ru/quicklook/";
        baseurlload += obj.id;
        string path = obj.id + ".png";
        WebClient Client = new WebClient();
        if (!File.Exists(path))
          Client.DownloadFile(new Uri(baseurlload), path);
        OnNewImageLoaded?.Invoke(path, obj);
      });
      //foreach(Object obj in root.objects)
      //{

      //}
    }

    #region request build helpers
    private static string AddAngles()
    {
      //angle__gte = 0 &
      //angle__lte = 10 &
      string result = "";
      result += "angle__gte=" + 0 + "&";
      result += "angle__lte=" + CRequestParams.MaxAngle + "&";
      return result;
    }
    private static string AddCloudness()
    {
      //cloudiness__gte = 0 &
      //cloudiness__lte = 20 &
      string result = "";
      result += "cloudiness__gte=" + 0 + "&";
      result += "cloudiness__lte=" + CRequestParams.MaxCludLevel + "&";
      return result;
    }
    private static string AddDates()
    {
      //date__gte=2016-09-17&
      //date__lte = 2017 - 09 - 17 &
      string result = "";
      result += "date__gte=" + CRequestParams.BeginDate.ToString("yyyy-MM-dd") + "&";
      result += "date__lte=" + CRequestParams.EndDate.ToString("yyyy-MM-dd") + "&";
      return result;
    }
    private static string AddSattelites()
    {
      //sensor__in=6&
      //sensor__in=57 &
      //sensor__in=42 &
      //sensor__in=14 &
      string result = "";
      foreach (CSatelite sat in CSatManager.Satelites.Where(s => s.IsSelected))
      {
        result += "sensor__in=" + sat.RequestName + "&";
      }
      return result;
    }
    private static string AddGeometry()
    {
      //geometry__intersects=MULTIPOLYGON+(((
      //50.3887939453125+58.53851965030045%2C+
      //50.3887939453125+58.594024186462185%2C+
      //50.56732177734375+58.594024186462185%2C+
      //50.56732177734375+58.53851965030045%2C+
      //50.3887939453125+58.53851965030045)))&
      string result = "";
      result += "geometry__intersects=MULTIPOLYGON+(((";
      for (int i = 0; i < 5; i++)
      {
        result += CRequestParams.Coords[2 * i] + "+" + CRequestParams.Coords[2 * i + 1];
        if (i != 4)
          result += "%2C+";
      }
      result += ")))&";
      result = result.Replace(',', '.');
      return result;
    }
    private static string AddSeasons()
    {
      //month__in=1&
      //month__in=2&
      //month__in=3&
      // .........
      //month__in=11&
      //month__in=12&
      string result = "";
      for (int i = 1; i <= 12; i++)
        result += "month__in=" + i + "&";
      return result;
    }
    private static string AddResponseParams()
    {
      //offset=0&
      //order_by=-date&
      //limit=50
      string result = "";
      result += "offset=0&";
      result += "order_by=-date&";
      result += "limit=500";
      return result;
    }
    #endregion



  }
}
