﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SatImgLoader
{
  /// <summary>
  /// Логика взаимодействия для ImgPreview.xaml
  /// </summary>
  public partial class ImgPreview : UserControl
  {
    Object info;
    public ImgPreview()
    {
      InitializeComponent();
    }
    public ImgPreview(string path, Object info):this()
    {
      this.info = info;
      string s = Directory.GetCurrentDirectory() + @"\" + path;
      try
      {
        PreviewImage.Source = new BitmapImage(new Uri(s, UriKind.RelativeOrAbsolute));
      }
      catch { }
      ImgInfo.Content = info.date.ToShortDateString() + " " + info.sensor.name;
    }
   
    private void OPenImageBtn_Click(object sender, RoutedEventArgs e)
    {
      PictureReview wnd = new PictureReview(info);
      wnd.ShowDialog();
    }
  }
}
