﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SatImgLoader
{

  public class Rootobject
  {
    public Meta meta { get; set; }
    public Object[] objects { get; set; }
  }

  public class Meta
  {
    public int limit { get; set; }
    public object next { get; set; }
    public int offset { get; set; }
    public object previous { get; set; }
    public int total_count { get; set; }
  }

  public class Object
  {
    private string _quicklook_url;
    public string angle { get; set; }
    public string catalog_id { get; set; }
    public int cloudiness { get; set; }
    public DateTime date { get; set; }
    public Geometry geometry { get; set; }
    public int id { get; set; }
    public string quicklook_url
    {
      get { return _quicklook_url; }
      set
      {
        _quicklook_url = value;
        if(value.EndsWith("png") || value.EndsWith("jpg") || value.EndsWith("jpeg") || value.EndsWith("tif") || value.EndsWith("tiff"))
        {
          IsPreviewAvalible = true;
        }
        else
        {
          IsPreviewAvalible = false;
        }
      }
    }
    public string resource_uri { get; set; }
    public Sensor sensor { get; set; }
      
    public bool IsPreviewAvalible = false;

  }

  public class Geometry
  {
    public float[][][] coordinates { get; set; }
    public string type { get; set; }
  }

  public class Sensor
  {
    public object coverage_band { get; set; }
    public string display_name { get; set; }
    public int id { get; set; }
    public string name { get; set; }
    public string resolution { get; set; }
    public string resource_uri { get; set; }
    public Satellite satellite { get; set; }
    public Type type { get; set; }
  }

  public class Satellite
  {
    public string description_url { get; set; }
    public string display_name { get; set; }
    public int id { get; set; }
    public string name { get; set; }
    public string resource_uri { get; set; }
  }

  public class Type
  {
    public int id { get; set; }
    public string name { get; set; }
    public string resource_uri { get; set; }
  }

}
