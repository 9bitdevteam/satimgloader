﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SatImgLoader
{
  /// <summary>
  /// Логика взаимодействия для DBRecordView.xaml
  /// </summary>
  public delegate void OnRecordViewRemovedDelegate(DBRecordView id);
  public partial class DBRecordView : UserControl
  {
    public event OnRecordViewRemovedDelegate OnRecordViewRemoved;
    private Object _info;
    public Object Info
    {
      get
      {
        return _info;
      }

      set
      {
        _info = value;
      }
    }
    public DBRecordView()
    {
      InitializeComponent();
    } 
    public DBRecordView(Object info) : this()
    {
      this.Info = info;
      string s = Directory.GetCurrentDirectory() + @"\" + CDBProvider.DataFolder + info.catalog_id + ".png";
      try
      {
        PreviewPic.Source = new BitmapImage(new Uri(s, UriKind.RelativeOrAbsolute));
      }
      catch { }
      SatName.Content += info.sensor.name;
      SensorResolution.Content += info.sensor.resolution;
      SensorType.Content += info.sensor.type.name;
      ShotAngle.Content += info.angle;
      ShotCatID.Content += info.catalog_id;
      ShotCloudness.Content += info.cloudiness.ToString();
      ShotDate.Content += info.date.ToLongDateString();
    }

    private void openBtn_Click(object sender, RoutedEventArgs e)
    {
      string argument = "/select, \"" + Directory.GetCurrentDirectory() + @"\" + CDBProvider.DataFolder + Info.catalog_id + ".png" + "\"";
      System.Diagnostics.Process.Start("explorer.exe", argument);
    }

    private void removeBtn_Click(object sender, RoutedEventArgs e)
    {
      string filename = Directory.GetCurrentDirectory() + @"\" + CDBProvider.DataFolder + Info.catalog_id + ".png.data";

      if (CDBProvider.Remove(filename))
      {
        this.Visibility = Visibility.Hidden;
        Thread.Sleep(300);
        OnRecordViewRemoved?.Invoke(this);
        MessageBox.Show("Снимок удален успешно");
      }
      else
        MessageBox.Show("При удалении снимка возникла ошибка, попробуйте позднее");

    }
  }
}
