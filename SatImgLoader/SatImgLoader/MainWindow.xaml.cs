﻿using GMap.NET.MapProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SatImgLoader
{
  /// <summary>
  /// Логика взаимодействия для MainWindow.xaml
  /// </summary>
  /// 
  public partial class MainWindow : Window
  {
    private DateTime _BeginDateTime = DateTime.Now.AddMonths(-1);
    private DateTime _EndDateTime = DateTime.Now;
    private int _ZoomLevel;
    private double _PositionLat;
    private double _PositionLong;

    private int _MaxCludLevel;
    private int _MinResolution;
    private int _MaxResolution;

    private int _LoadedCount = 0;
    private object LoadedCountLock = new object();

    DBExplorer dBExplorerWindow = null;

    public int ZoomLevel
    {
      get
      {
        return _ZoomLevel;
      }

      set
      {
        _ZoomLevel = value;
        if (ZommLbl != null)
          ZommLbl.Dispatcher.BeginInvoke(new Action<int>((lvl) => ZommLbl.Content = lvl.ToString()), value);
        if (ZoomSlider != null && ZoomSlider.Value != value)
          ZoomSlider.Dispatcher.BeginInvoke(new Action<int>((lvl) => ZoomSlider.Value = lvl), value);
        if (mapView != null && value != (int)mapView.Zoom)
          mapView.Zoom = value;
        CRequestParams.ZoomLevel = ZoomLevel;
        updateArea();
      }
    }

    public double PositionLat
    {
      get
      {
        return _PositionLat;
      }

      set
      {
        _PositionLat = value;
        LatitudeTB.Text = value.ToString();
      }
    }

    public double PositionLong
    {
      get
      {
        return _PositionLong;
      }

      set
      {
        _PositionLong = value;
        LongtitudeTB.Text = value.ToString();
      }
    }

    public DateTime EndDateTime
    {
      get
      {
        return _EndDateTime;
      }

      set
      {
        _EndDateTime = value;
        EndDate.DisplayDate = value;
        EndDate.SelectedDate = value;
        CRequestParams.EndDate = value;
      }
    }

    public DateTime BeginDateTime
    {
      get
      {
        return _BeginDateTime;
      }

      set
      {
        _BeginDateTime = value;
        BeginDate.SelectedDate = value;
        BeginDate.DisplayDate = value;
        CRequestParams.BeginDate = value;
      }
    }

    public int LoadedCount
    {
      get
      {
        return _LoadedCount;
      }

      set
      {
        _LoadedCount = value;
        CountOfLoadedLbl.Dispatcher.BeginInvoke(new Action<int>
          ((count) =>
       {
         CountOfLoadedLbl.Content = count;
       }), value
       );
      }
    }

    public MainWindow()
    {
      InitializeComponent();

    }

    private void mapView_Loaded(object sender, RoutedEventArgs e)
    {
      mapView.MapProvider = GMap.NET.MapProviders.YandexMapProvider.Instance;
      GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;

      mapView.Position = new GMap.NET.PointLatLng(54.629148, 39.734928); // Ryazan
      mapView.Bearing = 0;

      mapView.MinZoom = 2;
      mapView.MaxZoom = 17;
      mapView.Zoom = 10;
      mapView.OnMapZoomChanged += MapView_OnMapZoomChanged;
      mapView.OnPositionChanged += MapView_OnPositionChanged;

      mapView.Bearing = 0;
      //CanDragMap - Если параметр установлен в True, пользователь может перетаскивать карту с помощью правой кнопки мыши. 
      mapView.CanDragMap = true;
      //mapView.GrayScaleMode = true;
      //Устанавливаем центр приближения/удаления как курсор мыши.
      mapView.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;

      #region Old code
      //MarkersEnabled - Если параметр установлен в True, любые маркеры, заданные вручную будет показаны.
      //Если нет, они не появятся.
      //mapView.MarkersEnabled = true;

      ////Отказываемся от негативного режима.
      //mapView.NegativeMode = false;

      ////Разрешаем полигоны.
      //mapView.PolygonsEnabled = true;

      ////Разрешаем маршруты
      //mapView.RoutesEnabled = true;

      ////Скрываем внешнюю сетку карты с заголовками.
      //mapView.ShowTileGridLines = false;

      ////Указываем что все края элемента управления закрепляются у краев содержащего его элемента управления(главной формы), 
      ////а их размеры изменяются соответствующим образом.
      //mapView.Dock = DockStyle.Fill;


      //Если нам зачем-то понадобится прокси, то надо делать вот так:
      //
      //GMap.NET.MapProviders.GMapProvider.WebProxy = System.Net.WebRequest.GetSystemWebProxy();
      //GMap.NET.MapProviders.GMapProvider.WebProxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

      //choose your provider here
      //mapView.MapProvider = GMap.NET.MapProviders.OpenStreetMapProvider.Instance;
      #endregion

      mapView.MinZoom = 2;
      mapView.MaxZoom = 17;
      // whole world zoom
      mapView.Zoom = 10;
      // lets the map use the mousewheel to zoom
      mapView.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
      // lets the user drag the map
      mapView.CanDragMap = true;
      // lets the user drag the map with the left mouse button
      mapView.DragButton = MouseButton.Left;
      PositionLong = mapView.Position.Lng;
      PositionLat = mapView.Position.Lat;
      updateArea();
    }
    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      ZoomLevel = 7;
      MaxAngleScroll.Value = 4;
      MaxCloudScroll.Value = 10;

      BeginDate.SelectedDate = BeginDateTime;
      BeginDate.DisplayDate = BeginDateTime;

      EndDate.SelectedDate = EndDateTime;
      EndDate.DisplayDate = EndDateTime;

      CSatManager.Init();
      LoadSats();
      CImgLoader.OnNewImageLoaded += CImgLoader_OnNewImageLoaded;
      CImgLoader.OnNewImageSet += CImgLoader_OnNewImageSet;
      CImgLoader.OnLoadingFailed += CImgLoader_OnLoadingFailed;
      CDBProvider.init();
    }

    
    #region controls changed logic

    private void MapView_OnPositionChanged(GMap.NET.PointLatLng point)
    {
      PositionLong = point.Lng;
      PositionLat = point.Lat;
      updateArea();
    }

    private void MapView_OnMapZoomChanged()
    {
      ZoomLevel = (int)mapView.Zoom;
    }

    private void ZoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
      ZoomLevel = (int)ZoomSlider.Value;
      updateArea();
    }

    private void MaxCloudScroll_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
      CRequestParams.MaxCludLevel = (int)MaxCloudScroll.Value;
      MaxCloudTB.Content = CRequestParams.MaxCludLevel.ToString();
    }

    private void MaxAngleScroll_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
      CRequestParams.MaxAngle = (int)MaxAngleScroll.Value;
      MaxAngleTB.Content = CRequestParams.MaxAngle.ToString();
    }

    private void BeginDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
    {
      BeginDateTime = (DateTime)BeginDate.SelectedDate;
    }

    private void EndDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
    {
      EndDateTime = (DateTime)EndDate.SelectedDate;
    }
    #endregion

    private void LoadSats()
    {
      SatListContainer.Children.Clear();
      foreach (CSatelite sat in CSatManager.Satelites)
      {
        CheckBox cb = new CheckBox();
        cb.Content = sat.Name;
        cb.IsChecked = sat.IsSelected;
        SatListContainer.Children.Add(cb);
        cb.Checked += sat.Cb_Checked;
        cb.Unchecked += sat.Cb_Unchecked;
      }
    }

    private void SearchBtn_Click(object sender, RoutedEventArgs e)
    {
      if (EndDateTime < BeginDateTime)
      { MessageBox.Show("некорректная дата"); return; }
      string request = CImgLoader.BuildRequest();
      SearchBtn.IsEnabled = false;
      new Task(new Action(() => { CImgLoader.LoadData(request); })).Start();

    }
    
    private void updateArea()
    {
      if (mapView == null) return;
      /*
       порядок обхода:
       0========>1
       |         |
       |         |
       |         |
       3<========2
       */
      int i = -1;
      double left = mapView.ViewArea.Left;
      double right = mapView.ViewArea.Right;
      double top = mapView.ViewArea.Top;
      double bottom = mapView.ViewArea.Bottom;
      CRequestParams.Coords[++i] = left;
      CRequestParams.Coords[++i] = top;
      CRequestParams.Coords[++i] = right;
      CRequestParams.Coords[++i] = top;
      CRequestParams.Coords[++i] = right;
      CRequestParams.Coords[++i] = bottom;
      CRequestParams.Coords[++i] = left;
      CRequestParams.Coords[++i] = bottom;
      CRequestParams.Coords[++i] = left;
      CRequestParams.Coords[++i] = top;
    }

    private void CImgLoader_OnLoadingFailed(string cause = "")
    {
      this.Dispatcher.BeginInvoke(new Action(() =>
      {
        SearchBtn.IsEnabled = true;
      }));
    }

    private void CImgLoader_OnNewImageSet(int Count)
    {
      if(Count == 0)
      {
        this.Dispatcher.BeginInvoke(new Action(() =>
        {
          SearchBtn.IsEnabled = true;
        }));
        return;
      }
      ImagesContainer.Dispatcher.BeginInvoke(new Action(() =>
      {
        SearchBtn.IsEnabled = true;
        ImagesContainer.Children.Clear();
      }));
      CountOfAllPicturesLbl.Dispatcher.BeginInvoke(new Action<int>(
       (n) =>
       {
         lock (LoadedCountLock)
           LoadedCount = 0;
         CountOfAllPicturesLbl.Content = n;
       }
       ),
       Count);
      //throw new NotImplementedException();
    }

    private void CImgLoader_OnNewImageLoaded(string imagepath, Object sat)
    {
      lock (LoadedCountLock)
        LoadedCount++;
      ImagesContainer.Dispatcher.BeginInvoke(new Action<string, Object>(
        (imgpath, pictureinfo) =>
        {
          ImagesContainer.Children.Add(new ImgPreview(imgpath, pictureinfo));
        }
        ),
        imagepath, sat);

      //throw new NotImplementedException();
    }

    private void OpenDbBtn_Click(object sender, RoutedEventArgs e)
    {
      if(dBExplorerWindow != null)
      {
        dBExplorerWindow.Close();
      }
      dBExplorerWindow = new DBExplorer();
      dBExplorerWindow.Show();

    }
  }
}
